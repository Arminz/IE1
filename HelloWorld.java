// Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

// Extend HttpServlet class
public class HelloWorld extends HttpServlet {
 
   private String message;

   public void init() throws ServletException {
      // Do required initialization
      message = "Hello World2";
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      
      // Set response content type
      response.setContentType("text/html");

      // Actual logic goes here.
      PrintWriter out = response.getWriter();
      String page = 
         "‫<table>\n"
            +"<tr>\n"
               +"<th>Student-No</th>\n"
               +"<th>No</th>\n"
               +"<th>Grade</th>\n"
            +"</tr>\n"
            +"<tr>\n"
               +"<td>810199101</td>\n"
               +"<td>Ghamar</td>\n"
               +"<td>12.5</td>\n"
            +"</tr>\n"
            +"<tr>\n"
               +"<td>810199102</td>\n"
               +"<td>Ghamar</td>\n"
               +"<td>17.3</td>\n"
            +"</tr>\n"
            +"<tr>\n"
               +"<td>810199103</td>\n"
               +"<td>Ghodrat</td>\n"
               +"<td>8.5</td>\n"
            +"</tr>\n"
         +"</table>";
      out.println(page);
   }

   public void destroy() {
      // do nothing.
   }
}
